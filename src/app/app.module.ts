import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GuestComponentComponent } from './Component/guest-component/guest-component.component';
import { GuestNavBarComponent } from './Component/guest-nav-bar/guest-nav-bar.component';
import { GuestPageComponent } from './Component/guest-page/guest-page.component';


@NgModule({
  declarations: [
    AppComponent,
    GuestComponentComponent,
    GuestNavBarComponent,
    GuestPageComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
