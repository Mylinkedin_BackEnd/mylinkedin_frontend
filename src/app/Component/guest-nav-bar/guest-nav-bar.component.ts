import { Component, OnInit } from '@angular/core';
import { Company } from 'src/app/Model/company';

@Component({
  selector: 'app-guest-nav-bar',
  templateUrl: './guest-nav-bar.component.html',
  styleUrls: ['./guest-nav-bar.component.scss']
})
export class GuestNavBarComponent implements OnInit {

  //simuliano un array id ocmpany precaricato
  array:Company[]=[]
  constructor() {
    this.array.push(new Company(1,"Oracle"))
    this.array.push(new Company(2,"Coca Cola"))
    console.log("finito costruttore")
   }

  ngOnInit(): void {
  }

}
