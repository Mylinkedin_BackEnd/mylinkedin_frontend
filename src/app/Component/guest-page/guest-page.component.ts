import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/Model/post';

@Component({
  selector: 'app-guest-page',
  templateUrl: './guest-page.component.html',
  styleUrls: ['./guest-page.component.scss']
})
export class GuestPageComponent implements OnInit {

  posts:Post[]=[]
  coloreCard ={}

  constructor() {
    this.posts.push(new Post("Offerta Lavoro","lavora in ferrari"))
    this.posts.push(new Post("Event importante","Presentazione SEconda guerra mondiale"))
    this.posts.push(new Post("Richiesta lavoro","Interessato a lavorare in oracle"))
    this.posts.push(new Post("Richiesta lavoro","Interessato a lavorare in oracle"))
    this.posts.push(new Post("Richiesta lavoro","Interessato a lavorare in oracle"))

    this.posts.push(new Post("Offerta Lavoro","lavora in ferrari"))
    this.posts.push(new Post("Offerta Lavoro","lavora in ferrari"))
    this.posts.push(new Post("Offerta Lavoro","lavora in ferrari"))
    this.posts.push(new Post("Offerta Lavoro","lavora in ferrari"))
    this.posts.push(new Post("Offerta Lavoro","lavora in ferrari"))
    this.posts.push(new Post("Offerta Lavoro","lavora in ferrari"))
    this.posts.push(new Post("Richiesta lavoro","Interessato a lavorare in oracle"))

    this.posts.push(new Post("Richiesta lavoro","Interessato a lavorare in oracle"))

   }

  ngOnInit(): void {

  }

}
